---
layout: page
type: detail
title: Couleurs
group: styles
permalink: /styles/color.html
description: Color brings a design to life. Color is versatile; it's used to express emotion and tone, as well as place emphasis and create associations. Color should always be used in meaningful and intentional ways in order to create patterns and visual cues.

---


## Couleurs principales


{% include color-preview.html name="Bleu Nord" hexcode="#139fc6" %}
{% include color-preview.html name="Secondary" hexcode="#175d79" %}
{% include color-preview.html name="Tertiaire" hexcode="#06475E" %}
{% include color-preview.html name="Quaternaire" hexcode="#6c757d" %}
{% include color-preview.html name="Bleu" hexcode="#007bff" %}
{% include color-preview.html name="Success" hexcode="#28a745" %}
{% include color-preview.html name="Danger" hexcode="#dc3545" %}
{% include color-preview.html name="Warning" hexcode="#ffc107" %}
{% include color-preview.html name="Info" hexcode="#17a2b8" %}


## Couleurs additionelles


{% include color-preview.html name="White" hexcode="#fff" %}
{% include color-preview.html name="Gray 200" hexcode="#e9ecef" %}
{% include color-preview.html name="Gray 400" hexcode="#ced4da" %}
{% include color-preview.html name="Gray 600" hexcode="#6c757d" %}
{% include color-preview.html name="Gray 800" hexcode="#343a40" %}
{% include color-preview.html name="Black" hexcode="#000" %}

